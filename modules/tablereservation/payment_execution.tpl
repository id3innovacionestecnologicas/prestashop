<div class="container-fluid">
	<div class="row">
        <form action="#" role="form">
            <div class="form-group col-md-12">
                <label class="control-label col-md-4">Mensaje personalizado:</label>
                <textarea class="form-control" rows="3"></textarea>
            </div>
            <button type="submit" class="btn btn-default pull-right">Enviar</button>
        </form>
    </div>
</div>
