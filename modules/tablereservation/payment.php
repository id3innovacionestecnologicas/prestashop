<?php

include(dirname(__FILE__).'/../../config/config.inc.php');
include(dirname(__FILE__).'/../../header.php');
include(dirname(__FILE__).'/tablereservation.php');

if (!$cookie->isLogged())
    Tools::redirect('authentication.php?back=order.php');

$tablereservation = new TableReservation();

echo $tablereservation->execPayment($cart);

include_once(dirname(__FILE__).'/../../footer.php');

?>
