<?php
/**
* - By Ehinarr (ehinarr@msn.com)
* - THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

class TableReservation extends PaymentModule
{	
	private $_html = '';
	private $_postErrors = array();
	public  $currencies, $bf, $path;

	public function __construct()
	{
		$this->name = 'tablereservation';
		$this->displayName = 'tablereservation';
		$this->tab = 'Payment';
		$this->version = 1.0;
        $this->currencies = true;
        $this->currencies_mode = 'checkbox';
		
   		$this->bf = new Blowfish(_COOKIE_KEY_, _COOKIE_IV_);			//Create Blowfish Object for Encryption

        $config = Configuration::getMultiple(array('TABLE_RESERVATION_ID_ORDER_STATE'));
        if (isset($config['TABLE_RESERVATION_ID_ORDER_STATE']))
		$this->idOrderState = $config['TABLE_RESERVATION_ID_ORDER_STATE'];
   
        parent::__construct();

		/* The parent construct is required for translations */
		$this->page = basename(__FILE__, '.php');
		$this->displayName = $this->l('Table Reservation');
		$this->description = $this->l('Allows customers to reserve a table.');
        $this->path = $this->_path;
        
    }

	/**
	*	install()
	*	Called when 'Install' is clicked on module
	*/
	function install()
	{
		if (!parent::install()
                OR !$this->installDB()
                OR !$this->registerHook('payment')          	//Register Payment Hooks with Prestashop
			    OR !$this->registerHook('paymentReturn')
			    OR !$this->registerHook('adminOrder')
                OR !$this->_makeOrderState()

			)

            return true;
    }
    function uninstall()
	{
         if (!parent::uninstall()
		 OR !$this->uninstallDB()

		)
			return false;
		return true;
	}
 
    /**
	*	Create the table to store Table Reservation data
	*/
    function installDB()
    {
        Db::getInstance()->Execute('
        CREATE TABLE `'._DB_PREFIX_.'order_tablereservation_data` (
            `id_record` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	        `id_order` INT NOT NULL,
	        `data_string` TEXT NOT NULL,
	        `date_add` DATE,
	        `date_upd` DATE
         ) ENGINE = MYISAM;');
           return true;
	}
    /**
	*	Delete the table created to store Table Reservation data
	*/
    function uninstallDB()
	{
		Db::getInstance()->Execute('DROP TABLE `'._DB_PREFIX_.'order_tablereservation_data`;');
  		return true;
	}

    /**
	*	execPayment($cart)
	*	Called from payment.php
	*/
	function execPayment($cart)
    {
        //~ if (!$this->active)
			//~ return ;

		//~ $errTime     = '0';
		//~ $errNbpeople = '0';
		//~ $errDate     = '0';

		//~ if(isset($_POST['paymentSubmit']))
		//~ {
			//~ $trTime       	= $_POST['timeReserv_Hour'];
            //~ $trMin       	= $_POST['timeReserv_Minute'];
        	//~ $trNbpeople   	= $_POST['trNbpeople'];
        	//~ $trDate         = $_POST['dateReserv_Day'];
            //~ $trMonth         = $_POST['dateReserv_Month'];
         

            //~ if(empty($trTime))
				//~ $errTime = '1';
			//~ if(empty($trNbpeople) OR !is_numeric($trNbpeople))
				//~ $errNbpeople = '1';
            //~ if(empty($trDate))
               //~ $errTable = '1';

            //~ if($errTime == '1' || $errNbpeople == '1' || $errDate == '1')
            //~ {
                //~ global $cookie, $smarty;
                
                //~ $smarty->assign(array(
                //~ 'currencies'        => $this->getCurrency(),
                //~ 'errTime'			=> $errTime,
				//~ 'errNbpeople'		=> $errNbpeople,
				//~ 'errTable'			=> $errDate,
                //~ 'total' 			=> number_format($cart->getOrderTotal(true, 3), 2, '.', ''),
                //~ 'this_path' 		=> $this->_path,
				//~ 'this_path_ssl' 	=> Configuration::get('PS_FO_PROTOCOL').$_SERVER['HTTP_HOST'].__PS_BASE_URI__."modules/{$this->name}/"));


			//~ return $this->display(__FILE__, 'payment_execution.tpl');
			//~ }
			//~ else
			//~ {
                    //~ $trMin       = "{$trMin}<br/>";
                    //~ $trNbpeople  = "{$trNbpeople}<br/>";
                    //~ $trMonth     = "{$trMonth}<br/>";

				//~ /* Create Necessary variables for order placement */
					//~ $trString  = $this->l('Number of people:').'&nbsp;'.$trNbpeople;
                   	//~ $trString .= $this->l('Time:').'&nbsp;'.$trTime.':'.$trMin;
					//~ $trString .= $this->l('Day/Month:').'&nbsp;'.$trDate.'&nbsp;/&nbsp;'.$trMonth;

                  	//~ $trString  = pSQL($this->bf->encrypt($trString), true);

                    //~ $total = floatval(number_format($cart->getOrderTotal(true, 3), 2, '.', ''));

					//~ $this->validateOrder($cart->id, $this->idOrderState, $total, $this->displayName, null, $currency->id);

				//~ /* Insert Table Reservation String into the database for the current order.*/
					//~ $order = new Order($this->currentOrder);
					//~ $this->addDataString($order->id,  nl2br($trString));
					
				//~ /* Once complete, redirect to order-confirmation.php */	
					//~ Tools::redirectLink(__PS_BASE_URI__."order-confirmation.php?id_cart={$cart->id}&id_module={$this->id}&id_order={$this->currentOrder}&key={$order->secure_key}");
			//~ }
     //~ }
     //~ else
     //~ {
         //~ global $cookie, $smarty;
         
         //~ $smarty->assign(array(
         //~ 'currencies'    => $this->getCurrency(),
         //~ 'total' 		=> number_format($cart->getOrderTotal(true, 3), 2, '.', ''),
         //~ 'this_path' 	=> $this->_path,
         //~ 'this_path_ssl' => Configuration::get('PS_FO_PROTOCOL').$_SERVER['HTTP_HOST'].__PS_BASE_URI__."modules/{$this->name}/"));

         return $this->display(__FILE__, 'payment_execution.tpl');
		//~ }
	}

	/**
	*	hookPayment($params)
	*	Called in Front Office at Payment Screen
	*/
	function hookPayment($params)
	{
        if (!$this->active)
			return ;
   
		global $smarty;
		
		$smarty->assign(array(
            'this_path' 		=> $this->_path,
            'this_path_ssl' 	=> Configuration::get('PS_FO_PROTOCOL').$_SERVER['HTTP_HOST'].__PS_BASE_URI__."modules/{$this->name}/"));
			
		return $this->display(__FILE__, 'payment.tpl');
	}
	
	/**
	*	hookadminOrder($params)
	*	Called in Back Office upon Order Review
	*	Note: Only return data if an order is a Table Reservation order.
	*/
 
    function hookadminOrder($params)
	{
        if (!$this->active)
			return ;
   
		$id_order = $params['id_order'];

        if($this->isTableReservation($id_order))
        {
             if (isset($_GET['remData']))
             {
                 if(intval($_GET['remData']) == 1)
                 $this->removeDataString($id_order);
                 }

			global $smarty;
			$data_string = $this->getDataString($id_order);
			$smarty->assign(array(
               	'ccDataString' 		=> $this->bf->decrypt($data_string),
				'id_order'			=> $id_order,
				'this_page'			=> $_SERVER['REQUEST_URI'],
				'this_path' 		=> $this->_path,
            	'this_path_ssl' 	=> Configuration::get('PS_FO_PROTOCOL').$_SERVER['HTTP_HOST'].__PS_BASE_URI__."modules/{$this->name}/"));
			return $this->display(__FILE__, 'tablereservation.tpl');
		}
		else
			return "";
	}
    /**
	*	hookPaymentReturn($params)
	*	Called in Front Office upon order placement
	*/
	public function hookPaymentReturn($params)
	{
        if (!$this->active)
			return ;

		global $cookie, $smarty;
        $state       = $params['objOrder']->getCurrentState();
        $id_order    = $params['objOrder']->id;
        $products    = $params['objOrder']->getProducts();
        $this->_products = $products;
        $data_string = $this->getDataString($id_order);
		if ($state == _PS_OS_OUTOFSTOCK_ or $state == $this->idOrderState)
			$smarty->assign(array(
                'ccDataString' 	=> $this->bf->decrypt($data_string),
                'products'      => $products,
				'total_to_pay' 	=> Tools::displayPrice($params['total_to_pay'], $params['currencyObj'], false, false),
				'status' 		=> 'ok',
				'id_order' 		=> $params['objOrder']->id
			));
		else
			$smarty->assign('status', 'failed');

		return $this->display(__FILE__, 'payment_return.tpl');
	}

      public function displayConf()
	  {
        $this->_html .= '
		<div class="conf confirm">
			<img src="../img/admin/ok.gif" alt="'.$this->l('Confirmation').'" />
			'.$this->l('Settings updated').'
		</div>';
	}

     /**
	*	makeOrderState()
	*	An order state is necessary for this module to function.
	*	The id number of the order state is stored in a global configuration variable for use later
	*/
	private function _makeOrderState()
	{
		if(!(Configuration::get('TABLE_RESERVATION_ID_ORDER_STATE') > 0))
		{				
			$os = new OrderState();
			$os->name = array_fill(0,10,"Awaiting Table Reservation Validation");	//Fill with english language translation
			$os->send_mail = 0;
			$os->template = "";
			$os->invoice = 0;
			$os->color = "#33FF99";
			$os->unremovable = false;
			$os->logable = 0;		
			$os->add();
			Configuration::updateValue('TABLE_RESERVATION_ID_ORDER_STATE',$os->id);
		}
	}
		
	/**
	*	isTableReservation($id_order)
	*	Checks DB to see if an order used a tablereservationcard
	*/
	public function isTableReservation($id_order)
	{
		$db = Db::getInstance();
		$result = $db->getRow('
			SELECT * FROM `'._DB_PREFIX_.'order_tablereservation_data`
			WHERE `id_order` = "'.$id_order.'"');

		return intval($result["id_order"]) != 0 ? true : false;
		
	}
	
	/**
	*	removeDataString($id_order)
	*	Removes Table Reservation data from database upon order completion.
	*/
	public function removeDataString($id_order)
	{
		$removedString = pSQL($this->bf->encrypt("Table Reservation Information Has Been Removed."), true);
		$db = Db::getInstance();
		$result = $db->Execute('
		UPDATE `'._DB_PREFIX_.'order_tablereservation_data`
		SET `data_string` = "'.$removedString.'"
		WHERE `id_order` = "'.intval($id_order).'"');
	}
	
	/**
	*	addDataString($id_order, $data_string)
	*	Adds a data string to the database
	*/
	public function addDataString($id_order, $data_string)
	{
		$db = Db::getInstance();
		$result = $db->Execute('
		INSERT INTO `'._DB_PREFIX_.'order_tablereservation_data`
		( `id_order`, `data_string` )
		VALUES
		("'.intval($id_order).'","'.$data_string.'")');
	}
	
	/**
	*	getDataString($id_order)
	*	Returns the associated Table Reservation string from the database
	*/
	public function getDataString($id_order)
	{
		$db = Db::getInstance();
		$result = $db->ExecuteS('
		SELECT `data_string` FROM `'._DB_PREFIX_.'order_tablereservation_data`
		WHERE `id_order` ="'.intval($id_order).'";');
		return $result[0]['data_string'];
	}

}

?>
