<fieldset style="width: 400px;">

	<legend>

		<img src="{$this_path}logo.gif"/> {l s='Table Reservation Information' mod='tablereservation'}

	</legend>

	<input type="button" class="button" onclick="openCloseLayer('info')" value="{l s='Click Here to reveal the information.' mod='tablereservation'}" />

	<div id="info" style="display:none; border: solid red 1px;">

		{$ccDataString}

	</div>

	<input type="button" class="button" onclick="if(confirm('Are You Sure')) window.location = document.location + '&remData=1'" alt="{l s='Remove Table Reservation Data' mod='tablereservation'}" value="{l s='Remove Data' mod='tablereservation'}" />

</fieldset>
