{if $status == 'ok'}

	<p>{l s='Your Table Reservation on' mod='tablereservation'} <span class="bold">{$shop_name}</span> {l s='is now complete.' mod='tablereservation'}

		<br /><br />

		{l s='Once your reserve has been verified and have been accepted, a table will be reserved to you.' mod='tablereservation'}

         <h3>{l s='Order Details' mod='tablereservation'}</h3>

		- {l s='Total Payment Pending:' mod='tablereservation'} <span class="price">{$total_to_pay}</span><br /><br />
        - {l s='for a purchase of' mod='multibanco'} <span class="price"></span>
         <table class="std">
        <tbody>
		{foreach from=$products item=product name=products}{assign var='quantityDisplayed' value=0}{if $product.product_quantity > $quantityDisplayed}
		<tr class="{if $smarty.foreach.products.first}first_item{/if} {if $smarty.foreach.products.index % 2}alternate_item{else}item{/if}">
            <td><span class="order_qte_span editable">{$product.product_quantity|intval}</span></td>
            <td>{if $product.product_reference}{$product.product_reference|escape:'htmlall':'UTF-8'}{else}--{/if}</td>
			<td class="bold">{$product.product_name|escape:'htmlall':'UTF-8'}</td>
		</tr>
		{/if}
		{/foreach}
		</tbody>
	    </table>
        <br />
        <h3>{l s='Check out the data from your Table Reservation.' mod='tablereservation'}</h3>
       	 <span>{$ccDataString}<br /></span>


        <br /><br />{l s='For any questions or for further information, please contact our' mod='tablereservation'} <a href="{$base_dir}contact-form.php">{l s='customer support' mod='tablereservation'}</a>.

	</p>

{else}

	<p class="warning">

		{l s='We noticed a problem with your Table Reservation. If you think this is an error, you can contact our' mod='tablereservation'}

		<a href="{$base_dir}contact-form.php">{l s='customer support' mod='tablereservation'}</a>.

	</p>

{/if}

